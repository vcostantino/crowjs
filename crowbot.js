require('dotenv').config({ path: process.env.PWD+'/.env' });

const Discord = require('discord.js');
const bot = new Discord.Client();

const welcrom = [
	"Caw Caw Guys",
	"Caw",
	"CouwCouw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caw",
	"Caw Caaaaaw",
	"Bonjour Mathieu, comment vas-tu ?",
	"Vector > all, don't @ me"
];

const crowji = {
	'crowJS':['js','javascript','node','node.js'],
	'crowel':['nowel', 'crowel', 'noel', 'noël'],
	'crowLOL':['lol','mdr','rofl','ptdr','xptdr','ahaha','haha','oho','ihi','aha','ehe','uhu'],
	'crom':['com','crom','revolte'],
	'crowFor':['for','baleze'],
	'crowLove':['love','heart'],
	'crowGen':['gen','genius'],
	'crowBrain':['brain','smart'],
	'crowOkHand':['ok','nice','tmtc'],
	'crowThink':['think','mmh'],
	'crowW':['w','wall'],
	'crowfee':['fee','coffee','cawfee','crowfee','indeed']
};

bot.login(process.env.TOKEN);

bot.on('ready', () => {

	console.info(`Logged as ${bot.user.tag}`);
	//bot.channels.get('387169083029979140').send(welcrom[Math.floor(Math.random()*welcrom.length)]);
	bot.user.setActivity('Caw Caw');
});

function rndr () {

	let cr = Object.keys(crowji);
	let oc = cr[Math.floor(Math.random()*cr.length)];
	let ro = bot.emojis.find(emoji => emoji.name === oc)
	return ro ? ro.toString() : ':'+oc+':';
}

function bork (m, nms) {

	return m.then(nmsg => nmsg.edit(nms))
}

function broke (msg, key) {

	var m = msg.channel.send('craw');

	var nms = '';

	for (var k of key) {
		
		if (key.length < 2) {
			for (var x=0; x<3; x++){

				 m = bork(m, nms + rndr());
			}
		}
		nms += k + ' ';
	}

	bork(m, nms);
}


function testAuthor (msg) {

	if (msg.author.id == 425633965723090954) {
	

		if ((msg.content.indexOf("caw") > -1) && (msg.content.indexOf("?") > -1)) {
			return msg.channel.send("Caw !");
		} else {
			return Promise.resolve();
		}

	} else {
		return Promise.resolve();
	}

}





function crowprocess (msg) {

	var prc = msg;

	console.log(prc.content);


	if (msg.content == '!crow') {

		msg.channel.send(`
		**CAW** **__CAW__**
		
		caw caw **caw** caw **caw** **caw**
		caw cawcaw caw caw
		caw caw caw caw
		caw **caw**
		`);
		return;
	}

	if (msg.content == '!crow help') {

		msg.channel.send(`
		**CAW** **__CAW__**

		Caw Caw the text if it's contain only crow emojis termination
		`);
		return;
	}

	if (msg.content == '!crow killme') {

		msg.channel.send(`
		**CAW** **__CAW__**

		'crowJS' : ['js','javascript','node','node.js'],
		'crowel' : ['nowel', 'crowel', 'noel', 'noël'],
		'crowLOL': ['lol','mdr','rofl','ptdr','xptdr','ahaha','haha','oho','ihi','aha','ehe','uhu'],
		'crom' : ['com','crom','revolte'],
		'crowFor' : ['for','baleze'],
		'crowLove' : ['love','heart'],
		'crowGen' : ['gen','genius'],
		'crowBrain' : ['brain','smart'],
		'crowOkHand' : ['ok','nice','tmtc'],
		'crowThink' : ['think','mmh'],
		'crowW' : ['w','wall'],
		'crowfee' : ['fee','coffee','cawfee','crowfee','indeed']
		`);
		return;
	}

	let yo = msg.content.split(' ');

	//if(yo.length > 1) return;
	
	let oy = true;
	for (var k of yo) {
		let i = false;
		for (var key of Object.keys(crowji)) {
			if (crowji[key].indexOf(k.toLowerCase()) >= 0){
				i = true;
				break;
			}
		}
		oy = oy && i;
	}


	if (oy) {

		//prc.delete().catch(O_o=>{}); 
		
		prc.delete(); 
	
		nms = [];

		for (var k of yo) {
			
			for (var key of Object.keys(crowji)) {

				if (crowji[key].indexOf(k.toLowerCase()) >= 0){
		
					let ro = bot.emojis.find(emoji => emoji.name === key);
					ro = ro ? ro.toString() : ':'+k+':';
					nms.push(ro);
					
					break;
				}
			}
		}

		broke(prc, nms);
	}
	
}


bot.on('message', msg => {

	testAuthor(msg).then(res => {

		crowprocess(msg);
	});
	
});

function exitHandler(options, exitCode) {

	    if (options.clean) console.log('clean');

	    if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{clean:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));


